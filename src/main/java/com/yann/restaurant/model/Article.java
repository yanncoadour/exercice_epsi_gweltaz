package com.yann.restaurant.model;

import jakarta.persistence.*;

@Entity
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String designation;

    @Column(nullable = false)
    private Integer quantite;

    @Column(nullable = false)
    private Double prix;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getQuantity() {
        return quantite;
    }

    public void setQuantity(Integer quantity) {
        this.quantite = quantity;
    }

    public Double getPrice() {
        return prix;
    }

    public void setPrice(Double price) {
        this.prix = price;
    }
}