package com.yann.restaurant.service;

import com.yann.restaurant.model.Article;
import com.yann.restaurant.repository.ArticleRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    public Optional<Article> findById(Long id) {
        return articleRepository.findById(id);
    }

    public Article save(Article article) {
        return articleRepository.save(article);
    }

    public void deleteById(Long id) {
        articleRepository.deleteById(id);
    }

    public void decreaseQuantity(Long id, int quantity) {
        Optional<Article> optionalArticle = articleRepository.findById(id);

        if (optionalArticle.isPresent()) {
            Article article = optionalArticle.get();
            int newQuantity = article.getQuantity() - quantity;

            if (newQuantity < 0) {
                throw new RuntimeException("Insufficient stock for article: " + id);
            }

            article.setQuantity(newQuantity);
            articleRepository.save(article);
        } else {
            throw new RuntimeException("Article not found: " + id);
        }
    }
}
