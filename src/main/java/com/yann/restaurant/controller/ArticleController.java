package com.yann.restaurant.controller;

import com.yann.restaurant.model.Article;
import com.yann.restaurant.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping
    public List<Article> findAll() {
        return articleService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Article> findById(@PathVariable Long id) {
        Optional<Article> optionalArticle = articleService.findById(id);
        return optionalArticle.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public Article create(@RequestBody Article article) {
        return articleService.save(article);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Article> update(@PathVariable Long id, @RequestBody Article newArticle) {
        Optional<Article> optionalArticle = articleService.findById(id);
        if (optionalArticle.isPresent()) {
            Article article = optionalArticle.get();
            article.setDesignation(newArticle.getDesignation());
            article.setQuantity(newArticle.getQuantity());
            article.setPrice(newArticle.getPrice());
            return ResponseEntity.ok(articleService.save(article));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Optional<Article> optionalArticle = articleService.findById(id);
        if (optionalArticle.isPresent()) {
            articleService.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}

