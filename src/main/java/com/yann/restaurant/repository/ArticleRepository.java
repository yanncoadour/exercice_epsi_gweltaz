package com.yann.restaurant.repository;

import com.yann.restaurant.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Long> {
}
